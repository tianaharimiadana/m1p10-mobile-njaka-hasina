# m1p10-mobile-njaka-hasina

Project MOBILE(Mongodb, Express, Android Native), Master 1, Prom 10


## Description
Theme: application to enhance the tourism potential of Madagascar

## Authors
Thanks to all members for contributing to this project especially **Njaka RABEMANANTSOA** and **Hasina RAZAKAMANANTSOA**, student of the IT University Madagascar

## License
[MIT](https://choosealicense.com/licenses/mit/)
