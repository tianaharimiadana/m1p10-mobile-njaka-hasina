package com.example.mobile.data;

import com.example.mobile.service.ApiService;

import java.io.IOException;

public class DataManager {
    private ApiService apiService;

    public DataManager() {
        apiService = new ApiService();
    }

    public void getAllCategories() throws IOException {
        String url = "categories";
        apiService.get(url);
    }
}
