package com.example.mobile.service;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ApiService {
    String apiUrl = "https://mobile-webservice.vercel.app/api/v1/";
    private OkHttpClient client;
    private static final MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");

    public ApiService() {
        client = new OkHttpClient();
    }

    public String get(String url) throws IOException {
        Request request = new Request.Builder().url(getApiUrl() + url).build();

        try (Response response = client.newCall(request).execute()) {
            String responseBody = response.body().string();
            System.out.println(responseBody);
            return responseBody;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

//        Request request = new Request.Builder().url(getApiUrl() + url).build();
////        client.newCall(request).enqueue(new Callback() {
////            @Override
////            public void onFailure(@NonNull Call call, @NonNull IOException e) {
////                e.printStackTrace();
////            }
////
////            @Override
////            public String onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
////                System.out.println(response.body().string());
////                return response.body().string();
////            }
////        });
//        try {
//            Response response = client.newCall(request).execute();
//            System.out.println(response.body().string());
//            return response.body().string();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

//    public String post(String url, String json) throws IOException {
//        RequestBody requestBody = RequestBody.create(json, JSON_MEDIA_TYPE);
//        Request request = new Request.Builder().url(getApiUrl() + url).post(requestBody).build();
//
//        try (Response response = client.newCall(request).execute()) {
//            return response.body().string();
//        }
//    }
//
//    public String put(String url, String json) throws IOException {
//        RequestBody requestBody = RequestBody.create(json, JSON_MEDIA_TYPE);
//        Request request = new Request.Builder().url(getApiUrl() + url).put(requestBody).build();
//
//        try (Response response = client.newCall(request).execute()) {
//            return response.body().string();
//        }
//    }
//
//    public String delete(String url) throws IOException {
//        Request request = new Request.Builder().url(getApiUrl() + url).delete().build();
//
//        try (Response response = client.newCall(request).execute()) {
//            return response.body().string();
//        }
//    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
}
