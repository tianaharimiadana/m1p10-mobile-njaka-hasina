package com.example.mobile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mobile.service.ApiService;

import org.json.JSONArray;
import org.json.JSONException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link accueilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class accueilFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public accueilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment accueilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static accueilFragment newInstance(String param1, String param2) {
        accueilFragment fragment = new accueilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public int getDrawableResourceByName(String name) {
        return getResources().getIdentifier(name, "drawable", getContext().getPackageName());
    }

    public int getResourceIdByName(String resourceName, String resourceType) {
        return getResources().getIdentifier(resourceName, resourceType, getContext().getPackageName());
    }
        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accueil, container, false);
        try {
            String serverUrl = "sites";
            ApiService service = new ApiService();
            String response = service.get(serverUrl);
            JSONArray jsonArray = new JSONArray();
            if(response != null){
                jsonArray = new JSONArray(response);
            }
            List<String> dataList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                String data = jsonArray.getString(i);
                dataList.add(data);
            }


//               TextView text = rootView.findViewById(R.id.textView4);
//            JSONParser parser = new JSONParser();
//            JSONObject jsonObject = (JSONObject) parser.parse(dataList.get(0));
//            text.setText((String)jsonObject.get("nom_du_site"));
            for(int i = 1; i <= 6; i++){
                int textViewId = getResourceIdByName("textView" +i, "id");
                TextView text = rootView.findViewById(textViewId);
                Gson gson = new Gson();
                JsonObject jsonObject = gson.fromJson(dataList.get(i-1), JsonObject.class);
                text.setText(jsonObject.get("nom_du_site").getAsString());
                int imgViewId = getResourceIdByName("imageView" +i, "id");
                ImageView img = rootView.findViewById(imgViewId);
                int drawableResourceId = getDrawableResourceByName(jsonObject.get("image_principale_du_site").getAsString());
                img.setImageResource(drawableResourceId);
                final int var = i;
                text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // This code will be executed when the ImageView is clicked
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
//                        intent.putExtra("EXTRA_VARIABLE", dataList.get(var-1));
                        startActivity(intent);
                    }
                });

            }

//
        }

        catch (IOException e) {
            throw new RuntimeException(e);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        // Inflate the layout for this fragment
        return rootView;
    }
}