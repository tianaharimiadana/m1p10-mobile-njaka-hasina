package com.example.mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;

import com.example.mobile.data.DataManager;
import com.example.mobile.service.ApiService;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {

    private DataManager dataManager;

    public LoginActivity() {
        dataManager = new DataManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            dataManager.getAllCategories();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        setContentView(R.layout.activity_login);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
}